<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/article/{article}/like', [ApiController::class, 'addLike'])->name('like.add')->middleware("web");
Route::post('/article/{article}/dislike', [ApiController::class, 'deleteLike'])->name('like.delete')->middleware("web");
Route::get('/article/{article}/like', [ApiController::class, 'getLike'])->name('like.get')->middleware("web");
Route::post('/upload', [ApiController::class, 'apiUpload'])->name('api.upload');
