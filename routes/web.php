<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CommentaireController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\RlApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('public.home');
Route::get('/blog', [PublicController::class, 'blog'])->name('public.blog');
Route::get('/competive/shortly', [RlApiController::class, 'getEventShortly'])->name('public.competitive-shortly');
Route::get('/teams', [RlApiController::class, 'getTeams'])->name('public.teams');
Route::get('/competive/finish-or-in-progress', [RlApiController::class, 'getEventFinish'])->name('public.competitive-finish');
Route::get('/event-details/{id}', [RlApiController::class, 'getEventDetails'])->name('event.event-details');

Route::get('/login',[AuthenticatedSessionController::class, 'store'])->name('login');
Route::get('/register',[AuthenticatedSessionController::class, 'create'])->name('register');

Route::middleware('auth')->group(function () {
    Route::get('/articles/create', [UserController::class, 'create'])->name('articles.create');
    Route::post('/articles/store', [UserController::class, 'store'])->name('articles.store');
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard');
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/articles/{article}/edit', [UserController::class, 'edit'])->name('articles.edit');
    Route::post('/articles/{article}/update', [UserController::class, 'update'])->name('articles.update');
    Route::get('/articles/{article}/delete', [UserController::class, 'delete'])->name('articles.delete');
    Route::post('/comments/store', [CommentaireController::class, 'store'])->name('comments.store');
});

Route::get('/{user}', [PublicController::class, 'index'])->name('public.index');
Route::get('/{user}/{article}', [PublicController::class, 'show'])->name('public.show');

require __DIR__ . '/auth.php';
