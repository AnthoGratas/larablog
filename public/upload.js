document.getElementById('fileInput').addEventListener('change', function(event) {
    const file = event.target.files[0];
    const formData = new FormData();
    formData.append('file', file);

    fetch('/api/upload', {
        method: 'POST',
        body: formData,
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if (data.filename) {
            // Affichez le nom du fichier uploadé et mettez à jour le champ caché
            console.log('Uploaded filename:', data.filename);
            document.getElementById('imageName').value = data.filename; // Met à jour le champ caché avec le nom du fichier
        }
    })
    .catch(error => console.error('Error:', error));
});
