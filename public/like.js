import { createApp, ref } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

// On récupère l'élément HTML qui contient notre composant
// Nous en avons besoin pour récupérer les données de l'élément (data-id)
// et les passer au composant. (voir plus bas)
const mountEl = document.querySelector("#like");

createApp({
    /**
     * Une propriété props est une propriété qui est passée à un composant
     * depuis le composant parent. Ici, on récupère l'id de l'article.
     * Les props sont récupérées depuis les attributs HTML de l'élément HTML.
     * Ici, nous récupérons l'attribut "data-id" qui contient l'id de l'article.
     * Cette partie props est remplie par le {... mountEl.dataset} présent en bas du code
     */
    props: {
        id: {
            required: true
        }
    },

    setup(props) {
        // Compteur de like
        const count = ref("0");
        const isLiked = ref("false")

        // Récupération du nombre de like
        fetch(`/api/article/${props.id}/like`)
            .then(response => response.json())
            .then(data => { count.value = data.like; isLiked.value = data.liker })
        function like() {
            if (this.isLiked) {
                fetch(`/api/article/${props.id}/dislike`, { method: 'POST' })
                    .then(response => response.json())
                    .then(data => count.value = data.like)
                this.isLiked = false;

            } else {
                fetch(`/api/article/${props.id}/like`, { method: 'POST' })
                    .then(response => response.json())
                    .then(data => count.value = data.like)
                this.isLiked = true;
            }

        }
        return {
            count,
            like,
            isLiked
        }
    }
}, { ...mountEl.dataset }).mount(mountEl)