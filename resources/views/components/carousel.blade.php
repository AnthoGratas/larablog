<!-- Slider controls -->
<div class="carousel-controls">
    <button type="button" class="mx-5 absolute top-1/2 left-0 z-30 px-4 py-2 text-white bg-black bg-opacity-50 rounded-full cursor-pointer transform -translate-y-1/2 hover:bg-opacity-75 focus:outline-none" data-carousel-prev>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
        </svg>
        <span class="sr-only">Previous</span>
    </button>
    <button type="button" class="mx-5 absolute top-1/2 right-0 z-30 px-4 py-2 text-white bg-black bg-opacity-50 rounded-full cursor-pointer transform -translate-y-1/2 hover:bg-opacity-75 focus:outline-none" data-carousel-next>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
        </svg>
        <span class="sr-only">Next</span>
    </button>
</div>
