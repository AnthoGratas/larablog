<x-dash-layout>

    <div class="">
        <div class="max-w-7xl mx-auto pb-5 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-center pb-5 text-xl text-white leading-tight">
                Mes articles
            </h2>
            <!-- Message flash -->
            @if (session('success'))
            <div class="bg-green-500 text-white p-4 rounded-lg mt-6 mb-6 text-center">
                {{ session('success') }}
            </div>
            @endif
            @if (session('error'))
            <div class="bg-red-500 text-white p-4 rounded-lg mt-6 mb-6 text-center">
                {{ session('error') }}
            </div>
            @endif
            <div class="text-right pb-3">
                <a href="{{ route('articles.create') }}"
                    class="bg-[#E46A00] hover:bg-[#ffa14f] text-white font-bold py-2 px-4 rounded">
                    Créer un article
                </a>
            </div>
            @foreach ($articles as $article)
            <div class="bg-gray-50 overflow-hidden shadow-sm sm:rounded-lg mt-4">
                <div class="p-6 text-gray-900">
                    <h2 class="text-2xl font-bold">{{ $article->title }} {{ $article->draft ? " - Brouillon " : "" }}
                    </h2>
                    @foreach ($article->categories as $category)
                    <span class="bg-gray-100 text-gray-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded">#{{
            $category->name }}</span>
                    @endforeach
                    <p class="text-gray-700">{{ substr($article->content, 0, 100) }}...</p>
                    <div class="text-right">
                        <a href="{{ route('articles.edit', $article->id) }}"
                            class="text-blue-500 hover:text-blue-700"><i class="fa-solid fa-pen-to-square"></i></a>
                        <a href="{{ route('articles.delete', $article->id) }}"
                            class="text-red-500 hover:text-red-700"><i class="fa-regular fa-trash-can"></i></a>
                    </div>
                </div>

            </div>
            @endforeach
            @if(count($articles) == 0)
            <div class="text-center">
                <h2 class="font-semibold text-xl text-white leading-tight">
                    Vous n'avez aucun article publié ! Cliquez sur créer un article pour créer votre premier article !
                </h2>
            </div>
            @endif
        </div>
    </div>
    <!-- Articles -->

</x-dash-layout>