<x-guest-layout>
    <div class="text-center mb-8">
        <h2 class="font-semibold text-2xl text-white leading-tight">
            Bienvenue sur le blog de la communauté
        </h2>
        <br>

        <form action="{{ route('public.blog') }}" method="GET" class="max-w-lg mx-auto pb-4">
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only">Search</label>
            <div class="relative">
                <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                    </svg>
                </div>
                <input type="search" name="search" id="default-search"
                    class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                    placeholder="Vitality, Karmine Corp" />
                <button type="submit"
                    class="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2">Recherche</button>
            </div>
        </form>
    </div>

    @if(count($articles) > 0)
    <div class="grid grid-cols-1 sm:grid-cols-3 gap-6 mx-auto max-w-9xl px-4 my-4">
        <!-- Articles -->
        @foreach ($articles as $article)
        <div
            class="relative block overflow-hidden shadow-lg rounded-lg bg-[#E46A00] hover:bg-[#1082dc] text-white transition duration-300 ease-in-out transform hover:scale-105">
            <a href="{{ route('public.show', [$article->user_id, $article->id]) }}" class="block">
                <img src="{{ !is_null($article->image) ? asset('storage/images/' . $article->image) : 'https://www.rocketleague.com/_next/image?url=https%3A%2F%2Fmedia.graphassets.com%2Fresize%3Dfit%3Aclip%2Cheight%3A1080%2Cwidth%3A1920%2Foutput%3Dformat%3Awebp%2FIVqPjuXUTtWl2gGgkLfq&w=1920&q=75' }}"
                    alt="{{ $article->title }}" class="object-cover w-full h-48">
                <div class="p-4">
                    <h3 class="text-lg font-bold">{{ $article->title }}</h3>
                    <p class="text-sm">Par {{ $article->user->name }} | {{ $article->created_at->format('d M Y') }}</p>
                    <p class="text-xs mt-2">
                        Catégories :
                        @foreach ($article->categories as $category)
                        <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded">#{{
        $category->name }}</span>
                        @endforeach
                    </p>
                </div>
            </a>
            <div class="absolute bottom-0 right-0 bg-white text-black font-bold py-2 px-4 rounded-tl-lg text-center">
                <svg xmlns="http://www.w3.org/2000/svg" height="17" width="17" viewBox="0 0 512 512">
                    <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                    <path
                        d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
                </svg>
                <span>{{ count($article->likers) }}</span>
            </div>
        </div>
        @endforeach

    </div>
    @else
    <div class="text-center">
        <h2 class="font-semibold text-xl text-white leading-tight">
            Rien à voir ici ...
        </h2>
    </div>
    @endif
    <div class="justify-between px-8 mt-4">
        {{ $articles->appends(['search' => request()->search])->links() }}
    </div>
</x-guest-layout>