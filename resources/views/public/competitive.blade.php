<x-guest-layout>
    <div class="text-center mb-8">
        <h2 class="font-semibold text-xl text-white leading-tight">
            Liste des événements
        </h2>
        <br>
        <div class="text-center space-x-8">
            <x-nav-link :href="route('public.competitive-shortly', ['page' => 1])" :active="request()->routeIs('public.competitive-shortly')">
                À venir
            </x-nav-link>
            <x-nav-link :href="route('public.competitive-finish', ['page' => 1])" :active="request()->routeIs('public.competitive-finish')">
                En cour ou terminé
            </x-nav-link>
        </div>
    </div>
    <div class="container mx-auto px-4 mt-6">
        @if ($response)
        @php
        $data = json_decode($response, true);
        @endphp
        @if(isset($data['events']) && count($data['events']) > 0)
        <div class="grid grid-cols-1 md:grid-cols-4 gap-4">
            @foreach ($data['events'] as $event)
            @php
            $startDate = date('d/m/Y', strtotime($event['startDate']));
            $endDate = date('d/m/Y', strtotime($event['endDate']));
            @endphp
            <div class="block overflow-hidden shadow-lg rounded-lg bg-[#E46A00] hover:bg-[#1082dc] text-white transition duration-300 ease-in-out transform hover:scale-105">
                <a href="{{ url('/event-details', ['id' => $event['slug']]) }}" class="block">
                    <img class="w-full bg-gray-100 h-48 py-2 object-contain" src="{{ $event['image'] ?? 'https://cdn.freelogovectors.net/wp-content/uploads/2023/05/rocket-league_logo-freelogovectors.net_-640x400.png' }}" alt="Image de l'événement {{ $event['name'] }}">
                    <div class="p-4">
                        <h5 class="font-bold text-lg">{{ $event['name'] }}</h5>
                        <p class="text-sm">{{ $startDate }} au {{ $endDate }}</p>
                        <div class="mt-2">
                            <span class="text-sm">Région : {{ $event['region'] }}</span>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        @else
        <div class="text-center">
            <h2 class="font-semibold text-xl text-white leading-tight">
                Rien à voir ici ...
            </h2>
        </div>
        @endif
        <div class="flex justify-between items-center mt-6">
            <div>
                @if($page > 1 && count($data['events']) > 0)
                <button onclick="changePage(-1)" class="bg-[#1082dc] hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Retour
                </button>
                @elseif(count($data['events']) == 0 )
                <button onclick="goIndex()" class="bg-[#1082dc] hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Retour
                </button>
                @else
                <div class="flex-1"></div>
                @endif
            </div>

            <div>
                @if(count($data['events']) == 12 && $page > 0)
                <button onclick="changePage(1)" class="bg-[#1082dc] hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Suivant
                </button>
                @else
                <div class="flex-1"></div>
                @endif
            </div>
        </div>
        @endif
        <script>
            let currentPage = parseInt(new URLSearchParams(window.location.search).get('page'), 10);
            if (isNaN(currentPage) || currentPage < 1) {
                currentPage = 1;
            }

            function changePage(direction) {
                currentPage += direction;
                window.location.search = '?page=' + currentPage;
            }

            function goIndex() {
                window.location.search = '?page=1';
            }
        </script>
</x-guest-layout>