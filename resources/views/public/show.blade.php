<x-article-layout>
    <script type="module" src="{{ asset('like.js') }}"></script>
    <a href="javascript:history.go(-1)" class="text-blax-500 hover:text-black-700"><i
            class="fa-solid fa-arrow-left-long"></i></a>
    <div class="text-center">
        <h2 class="font-semibold text-xl text-gray-800  leading-tight">
            {{ $article->title }}
        </h2>
        @if (session('error'))
        <div class="bg-red-500 text-white p-4 rounded-lg mt-6 mb-6 text-center">
            {{ session('error') }}
        </div>
        @endif
        <div class="text-gray-500 text-sm">
            Publié le {{ $article->created_at->format('d/m/Y') }} par <a
                href="{{ route('public.index', $article->user->id) }}">{{ $article->user->name }}</a>
        </div>
        @foreach ($article->categories as $category)
        <span class="bg-gray-100 text-gray-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded  ">#{{
        $category->name }}</span>
        @endforeach
        <div class="block bg-white  overflow-hidden shadow-md rounded-lg mx-5 mt-5">
            <img src="{{ !is_null($article->image) ? asset('storage/images/' . $article->image) : 'https://www.rocketleague.com/_next/image?url=https%3A%2F%2Fmedia.graphassets.com%2Fresize%3Dfit%3Aclip%2Cheight%3A1080%2Cwidth%3A1920%2Foutput%3Dformat%3Awebp%2FIVqPjuXUTtWl2gGgkLfq&w=1920&q=75' }}"
                class="object-cover w-full h-70">
        </div>
    </div>


    <div>
        <div class="p-6 text-gray-900 ">
            <p class="text-gray-700 ">{{ $article->content }}</p>
        </div>
    </div>
    <div class="flex justify-end">
        @auth
        @if(auth()->user()->id != $article->user->id)
        <div id="like" data-id="{{ $article->id }}">
            <button @click="like()" class="block bg-white text-black font-bold py-2 px-4 rounded text-center">
                <svg v-if="!isLiked" xmlns="http://www.w3.org/2000/svg" height="17" width="17" viewBox="0 0 512 512">
                    <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                    <path
                        d="M225.8 468.2l-2.5-2.3L48.1 303.2C17.4 274.7 0 234.7 0 192.8v-3.3c0-70.4 50-130.8 119.2-144C158.6 37.9 198.9 47 231 69.6c9 6.4 17.4 13.8 25 22.3c4.2-4.8 8.7-9.2 13.5-13.3c3.7-3.2 7.5-6.2 11.5-9c0 0 0 0 0 0C313.1 47 353.4 37.9 392.8 45.4C462 58.6 512 119.1 512 189.5v3.3c0 41.9-17.4 81.9-48.1 110.4L288.7 465.9l-2.5 2.3c-8.2 7.6-19 11.9-30.2 11.9s-22-4.2-30.2-11.9zM239.1 145c-.4-.3-.7-.7-1-1.1l-17.8-20c0 0-.1-.1-.1-.1c0 0 0 0 0 0c-23.1-25.9-58-37.7-92-31.2C81.6 101.5 48 142.1 48 189.5v3.3c0 28.5 11.9 55.8 32.8 75.2L256 430.7 431.2 268c20.9-19.4 32.8-46.7 32.8-75.2v-3.3c0-47.3-33.6-88-80.1-96.9c-34-6.5-69 5.4-92 31.2c0 0 0 0-.1 .1s0 0-.1 .1l-17.8 20c-.3 .4-.7 .7-1 1.1c-4.5 4.5-10.6 7-16.9 7s-12.4-2.5-16.9-7z" />
                </svg>
                <svg v-else xmlns="http://www.w3.org/2000/svg" height="17" width="17" viewBox="0 0 512 512">
                    <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                    <path
                        d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
                </svg>
                <span>@{{ count }}</span>
            </button>
        </div>
        @else
        <div class="block bg-white text-black font-bold py-2 px-4 rounded text-center">
            <svg xmlns="http://www.w3.org/2000/svg" height="17" width="17" viewBox="0 0 512 512">
                <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                <path
                    d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
            </svg>

            <span>{{ count($article->likers) }}</span>
        </div>
        @endif
        @endauth
        @guest
        <div class="block bg-white text-black font-bold py-2 px-4 rounded text-center">
            <svg xmlns="http://www.w3.org/2000/svg" height="17" width="17" viewBox="0 0 512 512">
                <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                <path
                    d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
            </svg>

            <span>{{ count($article->likers) }}</span>
        </div>
        @endguest

    </div>
    <br>
    <hr>
    <br>
    <div>
        <div class="text-left">
            <h2 class="font-semibold text-xl text-gray-800  leading-tight">
                Commentaires
            </h2>
        </div>

        <br>
        @if (Route::has('login'))
        <div>
            @auth
            <div class="text-center">
                <button id="btn" onclick="showForm()"
                    class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 rounded-full border border-blue-500 hover:border-transparent rounded">
                    Ajouter un commentaire
                </button>
            </div>
            <br>
            <form action="{{ route('comments.store') }}" method="post" class="mt-6" id="comment" style="display: none;">
                @csrf
                <input type="hidden" name="article_id" value="{{ $article->id }}">
                <div class="p-6 pt-0 text-gray-900 ">
                    <textarea rows="5" name="content" style="resize:none" id="content" placeholder="Commentaires ..."
                        class="w-full  rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"></textarea>
                </div>
                <div class="text-right">
                    <button type="reset" onclick="hideForm()"
                        class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded">
                        Annuler
                    </button>
                    <button type="submit"
                        class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded">
                        Ajouter
                    </button>
                </div>
                <br>
                <hr>
                <br>
            </form>
            @endauth
        </div>
        @endif
        <!-- Liste des commentaires -->
        @foreach ($article->comments as $comment)
        <div>
            <div class="block max-w-2xl p-6 bg-white border border-gray-200 rounded-lg shadow">
                <div class="text-gray-500 text-sm">
                    Publié le {{ $comment->created_at->format('d/m/Y') }} par <a
                        href="{{ route('public.index', $article->user->id) }}">{{ $comment->user->name }}</a>
                </div>
                <p class="text-gray-700 ">{{ ($comment->content) }}</p>
            </div>
        </div>
        <br>
        @endforeach
        <hr>
    </div>

    <script>
    function showForm() {
        document.querySelector('#comment').style.display = 'block';
    }

    function hideForm() {
        document.querySelector('#comment').style.display = 'none';
    }
    </script>
</x-article-layout>