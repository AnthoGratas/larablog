<x-guest-layout>
    <div class="container mx-auto px-4">
        <div class="text-center mb-4">
            <h2 class="font-semibold text-xl text-white leading-tight">
                Équipes
            </h2>
        </div>

        <form action="{{ route('public.teams') }}" method="GET" class="max-w-md mx-auto pb-4">
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only">Search</label>
            <div class="relative">
                <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                    </svg>
                </div>
                <input type="search" name="search" id="default-search" class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500" placeholder="Vitality, Karmine Corp" />
                <button type="submit" class="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2">Recherche</button>
            </div>
        </form>

        @if ($teams->count() > 0)
        <!-- Grid for teams -->
        <div class="px-8 pb-3 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-4">
            @foreach ($teams as $team)
            <div class="relative bg-white shadow-md rounded-lg p-4 flex flex-col items-center group">
                <div class="w-full h-32 flex items-center justify-center">
                    <img src="{{ $team['team']['image'] ?? 'https://cdn.freelogovectors.net/wp-content/uploads/2023/05/rocket-league_logo-freelogovectors.net_-640x400.png' }}" alt="Logo de {{ $team['team']['name'] ?? 'Sans nom'}}" class="object-contain h-28 w-auto">
                </div>
                <div class="mt-2">
                    <h5 class="text-md text-center text-gray-700 font-semibold">
                        {{ $team['team']['name'] ?? 'Sans nom' }}
                    </h5>
                </div>
                <!-- Overlay content -->
                <div class="absolute inset-0 bg-white bg-opacity-90 flex flex-col justify-center items-center text-center scale-0 group-hover:scale-100 transition transform duration-300 ease-in-out">
                    <ul class="space-y-2">
                        @foreach ($team['players'] as $player)
                        <li class="flex items-center">
                            @php
                            $pays = strtoupper($player['country']);
                            if($pays == "EN" || $pays == "AB") $pays = "GB";
                            @endphp
                            <img src="https://flagsapi.com/{{ $pays }}/flat/64.png" width="16" height="12" alt="Drapeau" class="mr-2">
                            <span>{{ $player['tag'] }}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
        <div class="justify-between px-8 mt-4">
            {{ $teams->links() }}
        </div>
        @else
        <div class="text-center text-gray-500">
            Aucune équipe disponible.
        </div>
        @endif
    </div>
</x-guest-layout>
