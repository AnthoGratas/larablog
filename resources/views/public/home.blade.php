<x-guest-layout>
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-6  text-white">
        <h1 class="text-4xl font-bold text-center mb-8">Bienvenue sur RocketRealm</h1>

        <div class="text-center mb-8">
            <p class="text-lg px-6 py-3 inline-block">
                Découvrez tout sur Rocket League, des actualités récentes aux analyses approfondies, sans oublier les
                calendriers des compétitions à venir.
                Explorez <strong>RocketRealm</strong>, votre destination ultime pour tout ce qui concerne Rocket League.
                Plongez dans les dernières actualités, engagez-vous avec des articles de blog fascinants, et gardez un
                œil sur les compétitions à venir.
            </p>
        </div>
        <!-- Carousel pour les prochaines compétitions -->
        <section class="mb-12">
            <h2 class="text-2xl font-bold mb-6 text-center">Prochaines Compétitions</h2>
            <div id="default-carousel" class="relative w-full" data-carousel="slide">
                <div class="relative h-56 overflow-hidden rounded-lg md:h-96">
                    @if ($response)
                    @php
                    $data = json_decode($response, true);
                    @endphp
                    @if(isset($data['events']) && count($data['events']) > 0)
                    @foreach ($data['events'] as $event)
                    @php
                    $startDate = date('d/m/Y', strtotime($event['startDate']));
                    $endDate = date('d/m/Y', strtotime($event['endDate']));
                    @endphp
                    <div class="hidden duration-700 ease-in-out" data-carousel-item>
                        <a href="{{ route('event.event-details', ['id' => $event['slug']]) }}"
                            class="block w-full h-full relative">
                            <img src="{{ $event['image'] ?? 'https://cdn.freelogovectors.net/wp-content/uploads/2023/05/rocket-league_logo-freelogovectors.net_-640x400.png' }}"
                                class="bg-white w-full object-contain h-full py-4">
                            <div class="absolute bottom-0 left-0 right-0 bg-[#E46A00] bg-opacity-70 p-4">
                                <div class="text-white ">
                                    <h3 class="text-xl font-bold">{{ $event['name'] }}</h3>
                                    <p class="text-sm">{{ $startDate }} au {{ $endDate }}</p>
                                    <span class="text-xs">Région : {{ $event['region'] }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @endif
                    @endif
                </div>
                @include('components.carousel')
            </div>
        </section>


        <!-- Section pour les derniers articles de blog avec une présentation en cartes -->
        <section class="mb-12">
            <h2 class="text-2xl font-bold mb-6 text-center">Découvrez les derniers articles de la communauté</h2>
            <div class="grid grid-cols-1 md:grid-cols-3 gap-6">
                @foreach ($articles as $article)
                <a href="{{ route('public.show', [$article->user_id, $article->id]) }}"
                    class="block overflow-hidden shadow-lg rounded-lg bg-[#E46A00] hover:bg-[#1082dc] text-white transition duration-300 ease-in-out transform hover:scale-105">
                    <img src="https://www.rocketleague.com/_next/image?url=https%3A%2F%2Fmedia.graphassets.com%2Fresize%3Dfit%3Aclip%2Cheight%3A1080%2Cwidth%3A1920%2Foutput%3Dformat%3Awebp%2FIVqPjuXUTtWl2gGgkLfq&w=1920&q=75"
                        alt="{{ $article->title }}" class="object-cover w-full h-48">
                    <div class="py-3 px-4 ">
                        <h2 class="text-lg font-bold">{{ $article->title }}</h2>
                    </div>
                </a>
                @endforeach
            </div>
        </section>

    </div>
</x-guest-layout>