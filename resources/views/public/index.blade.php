<x-guest-layout>
    <div class="px-5">
        <a href="javascript:history.go(-1)" class="text-left text-white hover:text-black-700"><i
                class="fa-solid fa-arrow-left-long"></i></a>
    </div>

    <div class="text-center mb-8">
        <h2 class="font-semibold text-xl text-white leading-tight">
            Liste des articles publiés de {{ $user->name }}
        </h2>
    </div>

    <form action="{{ route('public.blog') }}" method="GET" class="max-w-lg mx-auto pb-4">
        <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only">Recherche</label>
        <div class="relative">
            <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                <svg class="w-5 h-5 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                    viewBox="0 0 24 24">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M21 21l-6-6m0 0A6 6 0 1 1 3 9a6 6 0 0 1 12 0Z" />
                </svg>
            </div>
            <input type="search" name="search" id="default-search"
                class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                placeholder="Rechercher des articles..." />
            <button type="submit"
                class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 transition duration-150 ease-in-out">Recherche</button>
        </div>
    </form>
    </div>

    <div class="grid grid-cols-1 sm:grid-cols-3 gap-6 mx-auto max-w-9xl px-4 my-4">
        <!-- Articles -->
        @foreach ($articles as $article)
        <a href="{{ route('public.show', [$article->user_id, $article->id]) }}"
            class="block overflow-hidden shadow-lg rounded-lg bg-[#E46A00] hover:bg-[#1082dc] text-white transition duration-300 ease-in-out transform hover:scale-105">
            <img src="{{ !is_null($article->image) ? asset('storage/images/' . $article->image) : 'https://www.rocketleague.com/_next/image?url=https%3A%2F%2Fmedia.graphassets.com%2Fresize%3Dfit%3Aclip%2Cheight%3A1080%2Cwidth%3A1920%2Foutput%3Dformat%3Awebp%2FIVqPjuXUTtWl2gGgkLfq&w=1920&q=75' }}"
                alt="{{ $article->title }}" class="object-cover w-full h-48">
            <div class="p-4">
                <h3 class="text-lg font-bold">{{ $article->title }}</h3>
                <p class="text-sm">Par {{ $article->user->name }} | {{ $article->created_at->format('d M Y') }}</p>
                <p class="text-xs mt-2">
                    Catégories :
                    @foreach ($article->categories as $category)
                    <span
                        class="inline-block bg-gray-200 rounded-full px-3 py-1 text-xs font-semibold text-gray-700 mr-2">#{{ $category->name }}</span>
                    @endforeach
                </p>
            </div>
        </a>
        @endforeach
    </div>
    <div class="justify-between px-8 mt-4">
        {{ $articles->appends(['search' => request()->search])->links() }}
    </div>
</x-guest-layout>