<x-dash-layout>
    <div class="text-center pb-6">
        <div class="px-5 text-left">
            <a href="javascript:history.go(-1)" class="text-left text-white hover:text-black-700"><i
                    class="fa-solid fa-arrow-left-long"></i></a>
        </div>
        <h2 class="font-semibold text-xl text-white leading-tight">
            Modifier un article
        </h2>
        @if (session('error'))
        <div class="bg-red-500 text-white p-4 rounded-lg my-6">
            {{ session('error') }}
        </div>
        @endif
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white overflow-hidden shadow-sm sm:rounded-lg p-6 text-gray-900">
        <form method="post" action="{{ route('articles.update', $article->id) }}" class="space-y-6">
            @csrf
            <div>

                <!-- Champ pour le titre de l'article -->
                <div>
                    <label for="title" class="block text-sm font-medium text-gray-700">Titre de l'article (max 30
                        caractères)</label>
                    <input type="text" name="title" id="title" value="{{ $article->title }}"
                        placeholder="Entrez le titre ici"
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                        maxlength="30">
                </div>

                <!-- Champ pour le contenu de l'article -->
                <div class="mt-4">
                    <label for="content" class="block text-sm font-medium text-gray-700">Contenu de l'article</label>
                    <textarea rows="10" name="content" id="content" placeholder="Écrivez votre contenu ici"
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{ $article->content }}</textarea>
                </div>

                <!-- Sélection des catégories -->
                <div class="mt-4">
                    <fieldset>
                        <legend class="text-sm font-medium text-gray-700">Catégories</legend>
                        <div class="mt-2 space-y-2">
                            @foreach ($categories as $category)
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input type="checkbox" name="categories[]" id="categorie_{{ $category->id }}"
                                        value="{{ $category->id }}"
                                        {{ in_array($category->id, $article->categories->pluck('id')->toArray()) ? 'checked' : '' }}
                                        class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="categorie_{{ $category->id }}"
                                        class="font-medium text-gray-700">{{ $category->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>

                <!-- Option brouillon -->
                <div class="mt-4">
                    <div class="flex items-center">
                        <input type="checkbox" name="draft" id="draft" {{ $article->draft ? 'checked' : '' }}
                            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                        <label for="draft" class="ml-2 block text-sm text-gray-900">
                            Enregistrer comme brouillon
                        </label>
                    </div>
                </div>

                <!-- Boutons du formulaire -->
                <div class="flex justify-end space-x-4 mt-4">
                    <button type="button" onclick="javascript:history.go(-1)"
                        class="bg-[#E46A00] hover:bg-[#ffa14f] text-white font-bold py-2 px-4 rounded">
                        Annuler
                    </button>
                    <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Modifier l'article
                    </button>
                </div>
            </div>
        </form>
    </div>
</x-dash-layout>