<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'RocketRealm') }}</title>

    <!-- Fonts -->
    <link rel="icon" type="image/x-icon" href="https://upload.wikimedia.org/wikipedia/commons/archive/c/c3/20200925050041%21Rocket_League_logo.svg">
    
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/2c7883ff58.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/dropzone/dist/dropzone.css">
    <script src="https://cdn.jsdelivr.net/npm/dropzone/dist/min/dropzone.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="font-sans text-gray-900 antialiased">

    @include('layouts.navigation')

    <!-- Page Heading -->
    @if (isset($header))
    <header class="bg-[#0c2043] shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            {{ $header }}
        </div>
    </header>
    @endif
    <div class="min-h-screen bg-[#0c2043]">
        <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
            <div class="bg-[#0c2043]">
                {{ $slot }}
            </div>
        </div>
    </div>
    @include('layouts.footer')
</body>

</html>