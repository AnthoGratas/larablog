<x-guest-layout>
    @php
    $event = json_decode($eventResponse, true);
    $participants = json_decode($participantsResponse, true);
    @endphp
    <div class="container mx-auto px-4 py-8">

        @if ($event)
        <div class="max-w-6xl mx-auto bg-white rounded-lg shadow-lg overflow-hidden">
            <div class="px-5 mt-4">
                <a href="javascript:history.go(-1)" class="text-left text-black-500 hover:text-black-700">
                    <i class="fas fa-arrow-left"></i> Retour
                </a>
            </div>


            <div class="flex flex-col items-center justify-center md:flex-row">
                <div class="flex-shrink-0">
                    <img src="{{ $event['image'] ?? 'https://cdn.freelogovectors.net/wp-content/uploads/2023/05/rocket-league_logo-freelogovectors.net_-640x400.png' }}"
                        alt="Image de couverture pour {{ $event['name'] }}"
                        class="px-5 pt-5 pb-5 h-48 w-full object-contain md:w-48">
                </div>
                <div class="p-8">
                    <h2 class="uppercase tracking-wide text-lg text-black font-semibold text-center">
                        {{ $event['name'] }}</h2>
                </div>
            </div>



            <div class="px-8 py-4 bg-[#E46A00] text-white flex justify-between items-center">
                <div class="mx-5 px-5 w-full">
                    <div class="mx-4 flex justify-center space-x-8">
                        <dl class="mt-1">
                            <dt class="font-semibold inline">Date :</dt>
                            <dd class="inline ml-2">{{ \Carbon\Carbon::parse($event['startDate'])->format('d/m/Y') }} -
                                {{ \Carbon\Carbon::parse($event['endDate'])->format('d/m/Y') }}</dd>
                        </dl>
                        <dl class="mt-1">
                            <dt class="font-semibold inline">Région :</dt>
                            <dd class="inline ml-2">{{ $event['region'] }}</dd>
                        </dl>
                        <dl class="mt-1">
                            <dt class="font-semibold inline">Cash Prize :</dt>
                            <dd class="inline ml-2">{{ number_format($event['prize']['amount'], 2) }}
                                {{ $event['prize']['currency'] }}</dd>
                        </dl>
                    </div>
                </div>
            </div>


            <div class="px-8 py-4">
                <h3 class="font-semibold text-lg text-gray-800">Étape(s) de l'événement</h3>
            </div>
            @php
            $nb = 1;
            @endphp
            <div class="px-8 pb-3 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                @foreach ($event['stages'] as $stage)
                <div class="bg-[#1082dc] text-white shadow-md rounded-lg p-4">
                    <h4 class="font-bold text-lg">{{$nb}} - {{$stage['name']}}</h4>
                    @php
                    $nb++;
                    @endphp
                    <p>Du {{ \Carbon\Carbon::parse($stage['startDate'])->format('d/m/Y') }}
                        au {{ \Carbon\Carbon::parse($stage['endDate'])->format('d/m/Y') }}</p>
                    @if (!empty($stage['location']))
                    <p>{{ $stage['location']['venue'] ?? 'N/A' }}, {{ $stage['location']['city'] ?? 'N/A' }},
                        {{ $stage['location']['country'] ?? 'N/A' }}</p>
                    @else
                    <p>Lieu non spécifié</p>
                    @endif
                    <p>Événement LAN : {{ $stage['lan'] ?? false ? 'Oui' : 'Non' }}</p>
                    @if(isset($stage['liquipedia']))
                    <div class="font-lighter italic">
                        <a href="{{ $stage['liquipedia'] }}" target="_blank" class="hover:underline">Plus de détails</a>
                    </div>
                    @endif
                </div>

                @endforeach
            </div>
            <div class="px-8 py-4">
                <h3 class="font-semibold text-lg text-gray-800">Participants</h3>
            </div>

            <div class="px-8 pb-3 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                @foreach ($participants['participants'] as $participant)
                <div class="relative bg-white shadow-md rounded-lg p-4 flex flex-col items-center group">
                    <div class="w-full h-32 flex items-center justify-center">
                        <img src="{{ $participant['team']['image'] ?? 'https://cdn.freelogovectors.net/wp-content/uploads/2023/05/rocket_league_logo_02-freelogovectors.net_.png' }}"
                            alt="Logo de {{ $participant['team']['name'] }}" class="object-contain h-28 w-auto">
                    </div>

                    <div class="mt-2">
                        <h5 class="text-md text-center text-gray-700 font-semibold">
                            {{ $participant['team']['name'] }}
                        </h5>
                    </div>
                    <div
                        class="absolute inset-0 bg-white bg-opacity-90 flex flex-col justify-center items-center text-center scale-0 group-hover:scale-100 transition transform duration-300 ease-in-out">
                        <ul class="space-y-2">
                            @foreach ($participant['players'] as $player)
                            <li class="flex items-center">
                                @php
                                $pays = strtoupper($player['country']);
                                if($pays == "EN" || $pays == "AB") $pays = "GB";
                                @endphp
                                <img src="https://flagsapi.com/{{ $pays }}/flat/64.png" width="16" height="12"
                                    alt="Drapeau" class="mr-2">
                                <span>{{ $player['tag'] }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>
            @if (count($participants['participants']) == 0)
            <div class="text-center text-gray-500 pb-5">
                Équipes participantes non déterminées.
            </div>
            @endif
        </div>
        @else
        <div class="text-center text-gray-500">
            Aucun détail d'événement disponible.
        </div>
        @endif
    </div>
</x-guest-layout>