<?php

namespace App\Http\Controllers;

use App\Models\Liker;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    public function store(Article $article)
    {
        $data["article_id"] = $article->id;
        $data['user_id'] = Auth::user()->id;

        $like = Liker::create($data);
        return redirect()->back();
    }
    
public function delete(Article $article)
{
    // On supprime l'entrée dans la table liker
    Liker::where('user_id', auth()->id())->where('article_id', $article->id)->delete();

    // On redirige l'utilisateur vers la liste des articles (avec un flash)
    return redirect()->back();
}
    

}
