<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;



class CommentaireController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->only(['article_id', 'content']);

        if (is_null($request['content'])) {
            return back()->withInput()->with('error', 'Merci de ne pas laisser de champ vide !');
        }

        $data['user_id'] = Auth::user()->id;

        $comment = Comment::create($data); 
        return redirect()->back();
    }
}
