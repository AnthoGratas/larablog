<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    public function index(User $user)
    {
        // On récupère les articles publiés de l'utilisateur
        $articles = Article::where('user_id', $user->id)->where('draft', 0)->paginate(12);

        // On retourne la vue
        return view('public.index', [
            'articles' => $articles,
            'user' => $user
        ]);
    }

    public function show(User $user, Article $article)
    {
        if ($article->draft == 0) {
            return view('public.show', [
                'article' => $article,
                'user' => $user
            ]);
        }
    }

    public function home()
    {

        $articles = Article::withCount('likers')
                            ->where('draft', 0)
                            ->orderByDesc('likers_count')
                            ->limit(6) 
                            ->get();
        
        $response = RlApiController::getTreeEventShortly();
        return view('public.home', ['articles' => $articles, 'response' => $response]);
    }

    public function blog(Request $request)
    {
        $perPage = 12;
    
        $articles = Article::withCount('likers')
                            ->where('draft', 0)
                            ->when($request->input('search'), function ($query) use ($request) {
                                return $query->where('title', 'like', '%' . $request->input('search') . '%');
                            })
                            ->orderByDesc('likers_count')
                            ->paginate($perPage);
    
        // Assurez-vous de renvoyer le paramètre de recherche à la vue pour pouvoir le réutiliser
        return view('public.blog', [
            'articles' => $articles,
            'search' => $request->input('search')
        ]);
    }
    



}
