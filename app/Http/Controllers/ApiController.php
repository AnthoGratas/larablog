<?php

namespace App\Http\Controllers;

use App\Models\Liker;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function getLike(Request $request, Article $article)
    {
        $userId = $request->user()->id;
        return response()->json(['like' => $article->likers->count(), 'liker' => $article->likers->contains('user_id', $userId)]);
    }

    public function addLike(Request $request, Article $article)
    {
        $userId = $request->user()->id;
        $data["article_id"] = $article->id;
        $data['user_id'] = $request->user()->id;

        Liker::create($data);
        return response()->json(['like' => $article->likers->count(), 'liker' => $article->likers->contains('user_id', $userId)]);
    }

    public function deleteLike(Request $request, Article $article)
    {
        $userId = $request->user()->id;
        $article_id = $article->id;

        // Recherche du like avec les clés primaires composées
        $like = Liker::where('user_id', $userId)
            ->where('article_id', $article_id)
            ->first();

        if ($like) {
            // Suppression du like
            $like->delete();
        }

        // Retourner la réponse JSON avec le nombre total de likes de l'article et un indicateur pour savoir si l'utilisateur actuel a aimé l'article ou non
        return response()->json([
            'like' => $article->likers->count(),
            'liker' => $article->likers->contains('user_id', $userId)
        ]);
    }

    public function apiUpload(Request $request)
    {
        $request->validate([
            'file' => 'required|file|image|max:2048', // Taille maximale de 2MB
        ]);
    
        $file = $request->file('file');
        $path = $file->store('public/images');  // Stocke le fichier dans le système de fichiers
        $filename = basename($path);            // Extrait le nom du fichier depuis le chemin
    
        return response()->json(['message' => 'File uploaded successfully!', 'path' => $path, 'filename' => $filename]);
    }
    
}
