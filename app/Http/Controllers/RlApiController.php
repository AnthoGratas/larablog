<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Hamcrest\Type\IsNumeric;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class RlApiController extends Controller
{

    function paginate($items, $perPage = 30, $page = null, $options = [])
    {
        if (!is_numeric($page)) {
            $page = 1;
        }
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->all(),
            $items->count(),
            $perPage,
            $page,
            $options
        );
    }


    public function getEventShortly(Request $request)
    {
        $page = $request->input('page', 1);
        if (!is_numeric($page) || (int)$page < 1) {
            $page = 1;
        } else {
            $page = (int)$page;
        }
        $url = "https://zsr.octane.gg/events";

        // Paramètres de la requête pour récupérer les 10 derniers événements
        $params = [
            'page' => $page,      // Numéro de la page
            'perPage' => 12,
            'sort' => "start_date:asc",
            'after' => date("Y-m-d"),

        ];

        // Initialisation de cURL
        $ch = curl_init();

        // Configuration de la requête cURL
        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Exécution de la requête
        $response = curl_exec($ch);

        // Vérification des erreurs
        if (curl_errno($ch)) {
            $response = 'Erreur CURL : ' . curl_error($ch);
        }

        curl_close($ch);

        return view('public.competitive', ["response" => $response, "page" => $page]);
    }

    public static function getTreeEventShortly()
    {
        $url = "https://zsr.octane.gg/events";

        // Paramètres de la requête pour récupérer les 10 derniers événements
        $params = [
            'page' => 1,      // Numéro de la page
            'perPage' => 10,
            'sort' => "start_date:asc",
            'after' => date("Y-m-d"),

        ];

        // Initialisation de cURL
        $ch = curl_init();

        // Configuration de la requête cURL
        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Exécution de la requête
        $response = curl_exec($ch);

        // Vérification des erreurs
        if (curl_errno($ch)) {
            $response = 'Erreur CURL : ' . curl_error($ch);
        }

        curl_close($ch);

        return $response;
    }

    public function getTeams(Request $request)
    {
        $url = "https://zsr.octane.gg/teams/active";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
    
        if (curl_errno($ch)) {
            $response = 'Erreur CURL : ' . curl_error($ch);
        }
        curl_close($ch);
    
        $teams = json_decode($response, true);

        if ($request->has('search') && $request->search != '') {
            $search = strtolower($request->search);
            $teams['teams'] = array_filter($teams['teams'], function ($team) use ($search) {
                return false !== stripos(strtolower($team['team']['name']), $search);
            });
        }
    
        if (isset($teams['teams'])) {
            $paginatedTeams = $this->paginate($teams['teams'], 30, $request->page);
            $paginatedTeams->withPath(route('public.teams'));
        } else {
            $paginatedTeams = collect();
        }
    
        return view('public.teams', ['teams' => $paginatedTeams]);
    }
    



    public function getEventFinish(Request $request)
    {
        $page = $request->input('page', 1);
        if (!is_numeric($page) || (int)$page < 1) {
            $page = 1;
        } else {
            $page = (int)$page;
        }
        $url = "https://zsr.octane.gg/events";

        // Paramètres de la requête pour récupérer les 10 derniers événements
        $params = [
            'page' => $page,      // Numéro de la page
            'perPage' => 12,
            'sort' => "start_date:desc",
            'before' => date("Y-m-d"),

        ];

        // Initialisation de cURL
        $ch = curl_init();

        // Configuration de la requête cURL
        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Exécution de la requête
        $response = curl_exec($ch);

        // Vérification des erreurs
        if (curl_errno($ch)) {
            $response = 'Erreur CURL : ' . curl_error($ch);
        }

        curl_close($ch);

        return view('public.competitive', ["response" => $response, "page" => $page]);
    }

    public function getEventDetails($id)
    {
        // Configuration initiale de cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Première requête pour obtenir les détails de l'événement
        $eventUrl = "https://zsr.octane.gg/events/{$id}";
        curl_setopt($ch, CURLOPT_URL, $eventUrl);
        $eventResponse = curl_exec($ch);
        if (curl_errno($ch)) {
            $eventResponse = 'Erreur CURL : ' . curl_error($ch);
        }

        // Deuxième requête pour obtenir des informations complémentaires
        $participantsUrl = "https://zsr.octane.gg/events/{$id}/participants";
        curl_setopt($ch, CURLOPT_URL, $participantsUrl);
        $participantsResponse = curl_exec($ch);
        if (curl_errno($ch)) {
            $participantsResponse = 'Erreur CURL : ' . curl_error($ch);
        }

        // Fermeture du gestionnaire cURL
        curl_close($ch);

        // Envoi des réponses à la vue
        return view('event.event-details', [
            'eventResponse' => $eventResponse,
            'participantsResponse' => $participantsResponse
        ]);
    }
}
