<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function create()
    {
        return view('articles.create', ['categories' => Category::all()]);
    }

    public function store(Request $request)
    {
        // Validation des entrées
        $validated = $request->validate([
            'title' => 'required|string|max:30',  // Longueur max ici corrigée
            'content' => 'required|string',
            'categories' => 'required|array',
            'categories.*' => 'exists:categories,id',
            'image' => 'nullable|string', // Validation pour le nom du fichier
            'draft' => 'boolean'
        ]);
    
        // Ajout de l'ID de l'utilisateur
        $validated['user_id'] = Auth::id();
    
        // Création de l'article
        $article = Article::create($validated);
    
        // Synchronisation des catégories
        $article->categories()->sync($request->input('categories'));
    
        // Redirection avec un message de succès
        return redirect()->route('dashboard')->with('success', 'Article créé avec succès !');
    }
    

    public function index()
    {
        // On récupère l'utilisateur connecté.
        $user = Auth::user();
        $articles = Article::where('user_id', $user->id)->get();
        // On retourne la vue.
        return view('dashboard', [
            'articles' => $articles
        ]);
    }

    public function edit(Article $article)
    {
        // On vérifie que l'utilisateur est bien le créateur de l'article
        if ($article->user_id !== Auth::user()->id) {
            return redirect()->route('dashboard')->with('error', 'Vous ne pouvez modifier que vos articles !');
        }

        // On retourne la vue avec l'article
        return view('articles.edit', [
            'article' => $article,
            'categories' => Category::all()
        ]);
    }

    public function update(Request $request, Article $article)
    {
        // Vérifier que l'utilisateur est bien le créateur de l'article
        if ($article->user_id !== $request->user()->id) {
            return redirect()->route('dashboard')->with('error', 'Vous ne pouvez modifier que vos articles !');
        }
        // Vérifier si l'un des champs est vide
        if (is_null($request['title']) || is_null($request['content'])) {
            return back()->withInput()->with('error', 'Merci de ne pas laisser de champ vide !');
        }

        if (strlen($request['title']) > 30) {
            return back()->withInput()->with('error', 'La longueur du titre de doit pas dépasser 50 caractères !');
        }

        $data = $request->only(['title', 'content', 'draft']);

        $data['user_id'] = Auth::user()->id;

        $data['draft'] = isset($data['draft']) ? 1 : 0;

        $article->update($data);
    
        
        $article->categories()->sync($request->input('categories'));

    
        return redirect()->route('dashboard')->with('success', 'Article mis à jour !');
    }
    

    public function delete(Request $request, Article $article)
    {
        // On vérifie que l'utilisateur est bien le créateur de l'article
        if ($article->user_id !== Auth::user()->id) {
            return redirect()->route('dashboard')->with('error', 'Vous ne pouvez supprimer que vos articles !');
        }

        // On met à jour l'article
        $article->delete();

        // On redirige l'utilisateur vers la liste des articles (avec un flash)
        return redirect()->route('dashboard')->with('error', 'Article supprimé !');
    }
}
