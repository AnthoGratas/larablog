<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * 
 * @property int $id
 * @property int|null $user_id
 * @property string $title
 * @property string $content
 * @property bool|null $draft
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $image
 * 
 * @property User|null $user
 * @property Collection|Category[] $categories
 * @property Collection|Comment[] $comments
 * @property Collection|Liker[] $likers
 *
 * @package App\Models
 */
class Article extends Model
{
	protected $table = 'articles';

	protected $casts = [
		'user_id' => 'int',
		'draft' => 'bool'
	];

	protected $fillable = [
		'user_id',
		'title',
		'content',
		'draft',
		'image'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

	public function comments()
	{
		return $this->hasMany(Comment::class);
	}

	public function likers()
	{
		return $this->hasMany(Liker::class);
	}
}
