<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Liker
 * 
 * @property int $user_id
 * @property int $article_id
 * @property int $id
 * 
 * @property Article $article
 * @property User $user
 *
 * @package App\Models
 */
class Liker extends Model
{
	protected $table = 'liker';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'article_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'article_id'
	];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
